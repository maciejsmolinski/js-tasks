# Equality Task

## Purpose

The task is about comparing two objects.

## Tips

* `API.areEqual` function must return true if both objects are equal (have the same structure and values).
* Please implement the `API.areEqual` function so your results match expected results.

## Legend

* Block of code marked with "Cannot be modified" flag should not be changed

```
// [Cannot be modified]
```

* Block of code marked with "Can be modified" flag can be changed

```
// [Can be modified]
```