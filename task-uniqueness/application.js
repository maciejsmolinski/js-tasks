/*jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */


// [Cannot be modified]
(function (API, $) {
  'use strict';

  /**
   * Collection of numbers
   *
   * @private
   */
  var numbers = [
    1, 5, 1, 8, 2, 8, 5, 1, 3
  ];

  /**
   * Prints numbers in [data-role="numbers-list"] element
   *
   * @public
   */
  API.printUniqueNumbers = function () {
    $('[data-role="numbers-list"]').html( '[ ' + API.unique(numbers).join(', ') + ' ]' );
  };

}(this, this.jQuery));



// [Can be modified]
(function (API) {
  'use strict';

  /**
   * Returns a collection of unique items
   *
   * @public
   */
  API.unique = function (collection) {
    return collection; // your implementation here
  };

  /**
   * Prints Unique Numbers in the source of the page
   *
   * @public
   */
  API.printUniqueNumbers();
}(this));
