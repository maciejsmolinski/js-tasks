# Transform Task

## Purpose

The task is about filtering out repeating items from an array.

## Tips

* `API.unique` function must return a collection (array) of unique items
* Please implement this function so your results match expected results

## Legend

* Block of code marked with "Cannot be modified" flag should not be changed

```
// [Cannot be modified]
```

* Block of code marked with "Can be modified" flag can be changed

```
// [Can be modified]
```