# Task - Weirdness of Functions

## Purpose

The task is about creating curried function - a function that returns function until it receives all parameters that original function takes.

## Tips

* The magic function should return function unless all parameters are provided
* In functional world this is called `currying`
* `API.sumThreeElements()()()(1)()()()(2)()()()(3)` should return same results as `API.sumThreeElements(1)(2)(3)` and `API.sumThreeElements(1,2)(3)` and `API.sumThreeElements(1,2,3)`

## Legend

* Block of code marked with "Cannot be modified" flag should not be changed

```
// [Cannot be modified]
```

* Block of code marked with "Can be modified" flag can be changed

```
// [Can be modified]
```