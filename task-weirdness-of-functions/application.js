/*jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */


// [Cannot be modified]
(function (API, $) {
  'use strict';

  /**
   * Sums three elements
   *
   * Sample Usage:
   *
   *   API.sumThreeElements(1,1,1) // => 3
   *   API.sumThreeElements(1,2,3) // => 6
   *
   * @public
   */
  API.sumThreeElements = function ( first, second, third) {
    return first + second + third;
  };

  /**
   * Prints test results
   *
   * @public
   */
  API.printResults = function() {
    var results = [];

    results.push('Result: ' + API.sumThreeElements(4,2,4) );
    results.push('Result: ' + API.sumThreeElements(3,1)(5) );
    results.push('Result: ' + API.sumThreeElements(6)(1,7) );
    results.push('Result: ' + API.sumThreeElements(0)(1)()(1) );

    $('[data-role="results-list"]').html(results.join('<br>'));
  };

}(this, this.jQuery));


// [Can be modified]
(function (API) {
  'use strict';


  // Your implementation of API.magicFunction
  API.magicFunction = function ( originalFunction ) {
    return originalFunction;
  };



  // Make API.sumThreeElements a magical (curried) function
  API.sumThreeElements = API.magicFunction(API.sumThreeElements);

  /**
   * Prints test results
   *
   * @public
   */
  API.printResults();
}(this, this.jQuery));
