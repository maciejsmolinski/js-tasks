/*global console:false */
/*jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */


// [Cannot be modified]
(function (global, $) {
  'use strict';

  var serverCallsMade = 0;

  $.ajaxSetup({
    complete : function () {
      $('[data-role="calls-counter"]').text(++serverCallsMade);
    }
  });

}(this, this.jQuery));


// [Can be modified]
(function (global, $) {
  'use strict';

  $('input').keypress(function () {

    $.ajax({
      url      : 'http://www.mocky.io/v2/5343b665ddc97c2711721b6b',
      success  : function () {
        console.log('Ajax call successfull');
      }
    });

  });

}(this, this.jQuery));
