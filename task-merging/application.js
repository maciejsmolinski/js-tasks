/*jshint camelcase:true, curly:true, eqeqeq:true, immed:true, indent:2, latedef:true, newcap:true, noarg:true, noempty:true, nonew:true, quotmark:single, undef:true, unused:true, strict:true, trailing:true */


// [Cannot be modified]
(function (API, $) {
  'use strict';

  /**
   * Returns array casted to string in a friendly form, e.g. "[1,2,3]"
   *
   * @public
   */
  Array.prototype.toString = function () {
    return '[' + this.join(',') + ']';
  };

  /**
   * Prints comparison results
   *
   * @public
   */
  API.printComparisonResults = function () {
    $('[data-role="merging-results"]').html( ([5,6,7].merge([1,2,3])).toString() );
  };

}(this, this.jQuery));



// [Can be modified]
(function (API) {
  'use strict';

  /**
   * Concatenates two arrays
   *
   * @public
   */
  Array.prototype.merge = function (anotherArray) {
    return this; // your implementation here;
  };

  /**
   * Prints comparison results in the page source
   *
   * @public
   */
  API.printComparisonResults();
}(this));
