# Equality Task

## Purpose

The task is about creating a prototype method that merges another array into original array

## Tips

* Please implement the `Array.prototype.merge` function so your results match expected results.

## Legend

* Block of code marked with "Cannot be modified" flag should not be changed

```
// [Cannot be modified]
```

* Block of code marked with "Can be modified" flag can be changed

```
// [Can be modified]
```