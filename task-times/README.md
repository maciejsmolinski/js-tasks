# Times Task

## Purpose

The task is about creating user friendly API by extending prototypes.

## Tips

* Your task is to create "times" function that is called on `5` in `application.js`
* Once implemented properly, the error in developer tools (console) should no longer appear
* Keep in mind extending prototypes is considered bad practice.
* This example is only to get as much flexibility of the language as possible.

## Legend

* Block of code marked with "Cannot be modified" flag should not be changed

```
// [Cannot be modified]
```

* Block of code marked with "Can be modified" flag can be changed

```
// [Can be modified]
```